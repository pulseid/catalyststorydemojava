package com.pulseid.catalyststory.offers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.util.AttributeSet;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PulseOffersView extends WebView {
    private OffersViewHandler offersViewHandlerj;
    private Map<String, String> headers = new HashMap<>();

    public PulseOffersView(@NonNull Context context) {
        this(context, null, -1);
    }

    public PulseOffersView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, -1);
    }

    public PulseOffersView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void initView(Context context) {
        setWebViewClient(initWebViewClient());
        WebSettings settings = getSettings();
        settings.setUseWideViewPort(true);
        settings.setJavaScriptEnabled(true);
        settings.setMixedContentMode(WebSettings.MIXED_CONTENT_NEVER_ALLOW);
        setOverScrollMode(OVER_SCROLL_NEVER);
        settings.setAllowFileAccess(false);
        addJavascriptInterface(new JSHandler(context), "PulseiD");

    }

    private WebViewClient initWebViewClient() {
        return new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                offersViewHandlerj.onOffersLoading();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                offersViewHandlerj.onOffersLoaded();
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                offersViewHandlerj.onErrorReceived();
            }

            @Override
            public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
                super.onReceivedHttpError(view, request, errorResponse);
                offersViewHandlerj.onHttpError(errorResponse.getStatusCode());
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if (isAcceptedUrl(request.getUrl()))
                    return super.shouldOverrideUrlLoading(view, request);
                else {
                    offersViewHandlerj.onErrorReceived();
                    return true;
                }
            }
        };
    }

    public void setOffersViewListener(OffersViewHandler offersViewHandlerj ) {
        this.offersViewHandlerj = offersViewHandlerj;
    }

    private boolean isAcceptedUrl(Uri requestUrl) {
        if (requestUrl != null && requestUrl.getScheme() != null) {
            return requestUrl.getScheme().equals("https");
        }
        return false;
    }

    /**
     * It will make the webview background transparent
     */
    public void makeBackgroundTransparent() {
        setBackgroundColor(Color.TRANSPARENT);
    }

    /**
     * @param headers is map of <String, String> to send app keys for authorization,
     *                It will be injected from the app
     */
    public void addHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    /**
     * @param url is the pulse offers url injected from the app.
     */
    public void loadPulseUrl(String url) {
        //WARNING:DO NOT REMOVE THIS AS THIS WILL BE USED TO TRACE ISSUE FOR DEBUGGING.
        headers.put("trace-id", UUID.randomUUID().toString());
        loadUrl(url, headers);
    }

    /**
     * @param json It is the json string of PulseConfig to change the UI of website like background color, border color.
     */
    public void updateConfig(String json) {
        evaluateJavascript("window.pulseConfig=$json", null);
    }
}
