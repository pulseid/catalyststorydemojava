package com.pulseid.catalyststory.offers;

public class PulseConfig {
    String backgroundColor = "";
    String borderColor = "";

    public PulseConfig(String backgroundColor, String borderColor) {
        this.backgroundColor = backgroundColor;
        this.borderColor = borderColor;
    }
}
