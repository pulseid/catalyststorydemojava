package com.pulseid.catalyststory.offers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class JSHandler {
    Context context;
    public static final String OFFER_ID = "offer_id";

    /**
     * Instantiate the interface and set the context
     */
    public JSHandler(Context c) {
        context = c;
    }

    @JavascriptInterface
    public void onClickOffer(String id) {
        Log.d("Offer Id", "" + id);
        Toast.makeText(context, "ID IS" + id, Toast.LENGTH_LONG).show();
        Intent intent = new Intent(context, PulseOfferStoryActivity.class);
        intent.putExtra(OFFER_ID, id);
        context.startActivity(intent);
    }
}