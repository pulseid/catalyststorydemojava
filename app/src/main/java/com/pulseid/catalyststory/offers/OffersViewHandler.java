package com.pulseid.catalyststory.offers;

public interface OffersViewHandler {
     void onOffersLoading();

    void onOffersLoaded();

    void onErrorReceived();

    void onHttpError(int statusCode);
}
