package com.pulseid.catalyststory.offers;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.pulseid.catalyststory.R;

import java.util.HashMap;

public class PulseOfferStoryActivity extends AppCompatActivity implements OffersViewHandler {
    private PulseOffersView pulseWebView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulse_offers_story);
        pulseWebView = findViewById(R.id.pulseView);
        setupPulseWebview();
    }

    private void setupPulseWebview() {
        String data = getIntent().getStringExtra(JSHandler.OFFER_ID);
        pulseWebView.setOffersViewListener(this);
        HashMap<String, String> map = new HashMap<>();
        map.put("X-Api-Key", "apiKey");
        map.put("X-Api-Secret", "apiKey");
        map.put("X-Api-UserId", "apiKey");
        pulseWebView.addHeaders(map);
        pulseWebView.loadPulseUrl("https://www.pulseid.com"+data);
    }

    @Override
    public void onOffersLoading() {
    }

    @Override
    public void onOffersLoaded() {
    }

    @Override
    public void onErrorReceived() {

    }

    @Override
    public void onHttpError(int statusCode) {

    }
}
