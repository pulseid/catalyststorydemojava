package com.pulseid.catalyststory;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.pulseid.catalyststory.offers.JSHandler;
import com.pulseid.catalyststory.offers.OffersViewHandler;
import com.pulseid.catalyststory.offers.PulseOffersView;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements OffersViewHandler {
    private PulseOffersView pulseWebView;
    private ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pulseWebView = findViewById(R.id.pulseView);
        progressBar = findViewById(R.id.progressBar);
        setupPulseWebview();
    }

    private void setupPulseWebview() {
        String data = getIntent().getStringExtra(JSHandler.OFFER_ID);
        pulseWebView.setOffersViewListener(this);
        HashMap<String, String> map = new HashMap<>();
        map.put("X-Api-Key", "apiKey");
        map.put("X-Api-Secret", "apiKey");
        map.put("X-Api-UserId", "apiKey");
        pulseWebView.addHeaders(map);
        pulseWebView.loadPulseUrl("https://www.pulseid.com");
    }

    @Override
    public void onOffersLoading() {
        pulseWebView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onOffersLoaded() {
        progressBar.setVisibility(View.GONE);
        pulseWebView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onErrorReceived() {

    }

    @Override
    public void onHttpError(int statusCode) {

    }
}
