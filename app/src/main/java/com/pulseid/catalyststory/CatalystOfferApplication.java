package com.pulseid.catalyststory;

import android.app.Application;

public class CatalystOfferApplication extends Application {
    private static CatalystOfferApplication application;

    public static CatalystOfferApplication getApplication() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }
}
