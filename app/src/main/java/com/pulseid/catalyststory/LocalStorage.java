package com.pulseid.catalyststory;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.pulseid.catalyststory.offers.R2D2;

class LocalStorage {

    private final SharedPreferences preferences;
    private static final String PREFS_NAME = "com.catalyst.pulseid.SharedPrefs";
    private static final String KEY_ALIAS = "key_alias";
    private final R2D2 r2d2;
    private static final String KEY_API = "key_api";
    private static final String KEY_APP_SECRET = "key_app_secret";
    private static LocalStorage instance = new LocalStorage();

    static LocalStorage getInstance() {
        return instance;
    }

    private LocalStorage() {
        preferences = CatalystOfferApplication.getApplication().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        r2d2 = new R2D2(CatalystOfferApplication.getApplication().getApplicationContext(), KEY_ALIAS);
    }

    void setApiKey(String apiKey) {
        String encrypted = r2d2.encryptData(apiKey);
        if (encrypted != null && !encrypted.equalsIgnoreCase("")) {
            apiKey = encrypted;
            Log.d("apikeyEncrpt", apiKey);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_API, apiKey);
        editor.apply();
    }

    String getApiKey() {
        String apiKey = preferences.getString(KEY_API, null);
        String decrypted = r2d2.decryptData(apiKey);
        if (decrypted != null && !decrypted.equalsIgnoreCase("")) {
            apiKey = decrypted;
        }
        return apiKey;
    }

    void setAppSecret(String appSecret) {
        String encrypted = r2d2.encryptData(appSecret);
        if (encrypted != null && !encrypted.equalsIgnoreCase("")) {
            appSecret = encrypted;
            Log.d("appSecretEncrpt", appSecret);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(KEY_APP_SECRET, appSecret);
        editor.apply();
    }

    String getAppSecret() {
        String appSecret = preferences.getString(KEY_APP_SECRET, null);
        String decrypted = r2d2.decryptData(appSecret);
        if (decrypted != null && !decrypted.equalsIgnoreCase("")) {
            appSecret = decrypted;
        }
        return appSecret;
    }
}

